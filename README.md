AVR-GCC-BUILDER
---

#Description

Collection of scripts to download and build the tools necessary to cross compile `c` code for Atmel AVR targets.

Using these scripts requires at least some knowledge of debugging build tools and fine tuning scripts if `make // curl // zsh` fails.
No extra debug help will be given if they don't work on your system.

#Instructions

The usual way to build everything is by calling the `build-all.sh` script. This will build and install all the tools in separate directories.
The output will be installed into the default `install-dir` directory in the project root.

call the build-all script like so

    ./build-all.sh

with DEBUG enabled for the building scripts

    BUILDER_DEBUG=asasdasd ./build-all.sh

with default install location for the avr-gcc tools, just prefix the call with the PREFIX variable defined with default location.

    PREFIX=avr-gcc-somewhere ./build-all.sh

with DOWNLOAD directory and BUILD directories specified as non default. **NOTE**: only the default directories are added to `.gitignore`

    DOWNLOAD_D=download-directory BUILD_D=build-artefactory ./build-all.sh


#Instructions for executing scripts individually

In the scripts folder are the build scripts for each component needed for the full toolchain. 
These scripts can be executed in their own context, 
    
    ./build-avrdude.sh

However the directories the script will use for downloads // building // installing will just be the directory that script was run in. 






