#!/bin/zsh
MESSAGES_SH=true
[ ! -z $BUILDER_DEBUG ] && echo "\033[40m\033[100mDEBUG\033[39m\033[49m \
Imported messages.sh"

ERROR_FG="\033[38;5;0m"
ERROR_BG="\033[48;5;1m"
DEFAULT_FG="\033[39m"
DEFAULT_BG="\033[49m"
MSG_FG="\033[30m"
MSG_BG="\033[104m"
DEBUG_FG="\033[40m"
DEBUG_BG="\033[100m"

COLOUR_ERROR="${ERROR_FG}${ERROR_BG}"
COLOUR_DEFAULT="${DEFAULT_FG}${DEFAULT_BG}"
COLOUR_DEBUG="${DEBUG_FG}${DEBUG_BG}"
COLOUR_MSG="${MSG_FG}${MSG_BG}"

NAME="OSX-AVR-TOOLS"

function msg() {
    echo "${COLOUR_MSG} MSG ${COLOUR_DEFAULT} OSX-AVR-TOOLS: $1"
}

function debug_msg() {
    [ ! -z ${BUILDER_DEBUG} ] && echo "${COLOUR_DEBUG}DEBUG${COLOUR_DEFAULT} OSX-AVR-TOOLS: $1"
}

function func_debug_start() {
    debug_msg "function() $1[start]"
    return 0
}

function func_debug_end() {
    debug_msg "function() $1[end]"
    return 0
}

function error_msg() {
    echo "${COLOUR_ERROR}ERROR${COLOUR_DEFAULT} OSX-AVR-TOOLS: $1"
}

function print_variables() {

    for VAR in $* ; do
        eval "VARINVAR=\"\${${VAR}}\""
        debug_msg "$VAR: ${VARINVAR}"
    done
}
