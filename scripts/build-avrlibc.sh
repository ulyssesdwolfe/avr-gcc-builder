#!/bin/zsh

# Script to download and build AVR LIB C for OSX so that we may have access to it for the other tools.

BUILD_AVRLIBC_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}
: ${TARGET:=avr}

# import scripts
[[ -z $MESSAGES_SH ]] && source $BUILD_SCRIPT_D/messages.sh
[[ -z $VARIABLES_SH ]] && source $BUILD_SCRIPT_D/variables.sh

# get the src
AVRLIBC_SRC_PATH="${DOWNLOAD_D}/${AVR_LIBC_SRC}"
AVRLIBC_SIG_PATH="${DOWNLOAD_D}/${AVR_LIBC_SRC}.sig"
[[ ! -a ${AVRLIBC_SRC_PATH} ]] && curl ${AVR_LIBC_SRC_URL} -o ${AVRLIBC_SRC_PATH}
echo $AVR_LIBC_SRC_URL
[[ ! -a ${AVRLIBC_SIG_PATH} ]] && curl ${AVR_LIBC_SIG_URL} -o ${AVRLIBC_SIG_PATH}

#verify
[[ $VERIFY = "true" ]] && gpg  --verify ${AVRLIBC_SIG_PATH} ${AVRLIBC_SRC_PATH} && if [ ! $? -eq 0 ] ; then ;
     error_msg "AVRLIBC SRC TAR did not match the sig file. Error $?" ; fi

cd $BUILD_D

[[ ! -d $BUILD_D/$AVR_LIBC_NAME ]] && tar -xjvf $AVRLIBC_SRC_PATH

[[ ! -d $AVR_LIBC_BUILD_D ]] && mkdir $AVR_LIBC_BUILD_D
cd $AVR_LIBC_BUILD_D 

eval ../configure --prefix=$INSTALL_D --build=`./config.guess` --host=${TARGET}
[[ $? > 0 ]] && error_msg "Configure script for $AVR_LIBC_NAME failed." && exit 1

$GNU_MAKE
[[ $? > 0 ]] && error_msg "Make script for $AVR_LIBC_NAME failed." && exit 1

$GNU_MAKE check
[[ $? > 0 ]] && error_msg "Make check script for $AVR_LIBC_NAME failed." && exit 1

$GNU_MAKE install
[[ $? > 0 ]] && error_msg "Make Install script for $AVR_LIBC_NAME failed." && exit 1

cd $PRJ_ROOT
