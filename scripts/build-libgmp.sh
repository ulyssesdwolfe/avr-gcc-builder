#!/bin/zsh

# Script to download and build GNU GMP Library for OSX so that we may have access to it for the other tools.
# Default OSX GMP Library is missing the AVR target functionality

BUILD_LIBGMP_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}

# import scripts
[[ -z $MESSAGES_SH ]] && source $BUILD_SCRIPT_D/messages.sh
[[ -z $VARIABLES_SH ]] && source $BUILD_SCRIPT_D/variables.sh

# get the src
LIBGMP_SRC_PATH="${DOWNLOAD_D}/${GNU_GMP_SRC}"
LIBGMP_SIG_PATH="${DOWNLOAD_D}/${GNU_GMP_SRC}.sig"
[[ ! -a ${LIBGMP_SRC_PATH} ]] && curl ${GNU_GMP_SRC_URL} -o ${LIBGMP_SRC_PATH}
[[ ! -a ${LIBGMP_SIG_PATH} ]] && curl ${GNU_GMP_SIG_URL} -o ${LIBGMP_SIG_PATH}

#verify
[[ $VERIFY = "true" ]] && gpg  --verify ${LIBGMP_SIG_PATH} ${LIBGMP_SRC_PATH} && if [ ! $? -eq 0 ] ; then ;
     error_msg "LIBGMP SRC TAR did not match the sig file. Error $?" ; fi

cd $BUILD_D

[[ ! -d $BUILD_D/$GNU_GMP_NAME ]] && tar -xvf $LIBGMP_SRC_PATH

[[ ! -d $GNU_GMP_BUILD_D ]] && mkdir $GNU_GMP_BUILD_D
cd $GNU_GMP_BUILD_D 

eval ../configure --prefix=$INSTALL_D --disable-shared --enable-static

[[ $? > 0 ]] && error_msg "Configure script for $GNU_GMP_NAME failed." && exit 1

$GNU_MAKE
[[ $? > 0 ]] && error_msg "Make script for $GNU_GMP_NAME failed." && exit 1

$GNU_MAKE check
[[ $? > 0 ]] && error_msg "Make check script for $GNU_GMP_NAME failed." && exit 1

$GNU_MAKE install
[[ $? > 0 ]] && error_msg "Make Install script for $GNU_GMP_NAME failed." && exit 1

cd $PRJ_ROOT
