#!/bin/zsh

# Script to download and build AVARICE DEBUG Tool for OSX so that we may have access to it for the other tools.
# #############################################################################
# Initialise the script
# #############################################################################

BUILD_AVARICE_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}

# These variables can be passed to the script externally but if not then are initialised to some default value
: ${TARGET:=avr}
: ${PRJ_ROOT:=$(pwd)}
: ${DOWNLOAD_D:="$PRJ_ROOT/downloads"}
: ${BUILD_D:=build-dir}
: ${INSTALL_D:="$PRJ_ROOT/install-dir"}
: ${LOGS:=$PRJ_ROOT/logs}
: ${VERIFY:=false}
: ${GNU_MAKE:=make}
: ${AVARICE_VERSION:="2.13"}
: ${AVARICE_NAME:="avarice-$AVARICE_VERSION"}
: ${AVARICE_SRC:="$AVARICE_NAME.tar.bz2"}
: ${AVARICE_SRC_URL:="https://sourceforge.net/projects/avarice/files/avarice/$AVARICE_NAME/$AVARICE_SRC/download"}
: ${AVARICE_SRC_PATH:="$DOWNLOAD_D/$AVARICE_SRC"}
: ${AVARICE_SHA1="b0bc56d587600651c0e61be676177b2eebfad3ae"}
: ${AVARICE_SIG_URL:=""}
: ${AVARICE_SIG_PATH:=""}
: ${AVARICE_BUILD_D:="$AVARICE_NAME/$BUILD_D"}


LOG_FILE="${LOGS}/$THIS_SCRIPT.log"

# #############################################################################
# Setup directories
# #############################################################################
[[ ! -d ${DOWNLOAD_D} ]] && mkdir ${DOWNLOAD_D}
[[ ! -d ${BUILD_D} ]] && mkdir ${BUILD_D}
[[ ! -d ${LOGS} ]] && mkdir ${LOGS}
# #############################################################################
# Download the source
# #############################################################################
[[ ! -a ${AVARICE_SRC_PATH} ]] && curl -L ${AVARICE_SRC_URL} -o ${AVARICE_SRC_PATH}
# get the sig/sha file
if [ $VERIFY = "true" ] ; then
    [[ ! -a ${AVARICE_SIG_PATH} ]] && \
        curl ${AVARICE_SIG_URL} -o ${AVARICE_SIG_PATH}
fi

# #############################################################################
# Verify the source
# #############################################################################
if [ $VERIFY = "true" ] ; then
    gpg  --verify ${AVARICE_SIG_PATH} ${AVARICE_SRC_PATH}
    if [ ! $? -eq 0 ] ; then
        error_msg "AVARICE SRC TAR did not match the sig file. Error $?"
    fi
fi



# #############################################################################
# Decompress the source
# #############################################################################
cd $BUILD_D
tar -xjvf $AVARICE_SRC_PATH

[[ ! -d $AVARICE_BUILD_D ]] && mkdir $AVARICE_BUILD_D
cd $AVARICE_BUILD_D 


# #############################################################################
# Configure the project
# #############################################################################
eval ../configure --prefix=$INSTALL_D \
    "LDFLAGS=\"-L/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/\"" >> ${LOG_FILE}

[[ $? > 0 ]] && error_msg "Configure script for $AVARICE_NAME failed." && exit 1


# #############################################################################
# Build the project
# #############################################################################
$GNU_MAKE >> $LOG_FILE
[[ $? > 0 ]] && error_msg "Make script for $AVARICE_NAME failed." && exit 1

$GNU_MAKE check >> $LOG_FILE
[[ $? > 0 ]] && error_msg "Make check script for $AVARICE_NAME failed." && exit 1

$GNU_MAKE install >> $LOG_FILE
[[ $? > 0 ]] && error_msg "Make Install script for $AVARICE_NAME failed." && exit 1

cd $PRJ_ROOT
