#!/bin/zsh

# Script to download and build GNU MPR LIB for OSX so that we may have access to it for the other tools.
# Default OSX mpr is missing the AVR target functionality

BUILD_LIBMPFR_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}
: ${TARGET:=avr}

# import scripts
[[ -z $MESSAGES_SH ]] && source $BUILD_SCRIPT_D/messages.sh
[[ -z $VARIABLES_SH ]] && source $BUILD_SCRIPT_D/variables.sh

# get the src
LIBMPFR_SRC_PATH="${DOWNLOAD_D}/${GNU_MPFR_SRC}"
LIBMPFR_SIG_PATH="${DOWNLOAD_D}/${GNU_MPFR_SRC}.sig"
[[ ! -a ${LIBMPFR_SRC_PATH} ]] && curl ${GNU_MPFR_SRC_URL} -o ${LIBMPFR_SRC_PATH}
[[ ! -a ${LIBMPFR_SIG_PATH} ]] && curl ${GNU_MPFR_SIG_URL} -o ${LIBMPFR_SIG_PATH}

#verify
[[ $VERIFY = "true" ]] && gpg  --verify ${LIBMPFR_SIG_PATH} ${LIBMPFR_SRC_PATH} && if [ ! $? -eq 0 ] ; then ;
     error_msg "LIB MPFR SRC TAR did not match the sig file. Error $?" ; fi

cd $BUILD_D

[[ ! -d $BUILD_D/$GNU_MPFR_NAME ]] && tar -xvf $LIBMPFR_SRC_PATH

[[ ! -d $GNU_MPFR_BUILD_D ]] && mkdir $GNU_MPFR_BUILD_D
cd $GNU_MPFR_BUILD_D 

eval ../configure --prefix=$INSTALL_D --target=${TARGET} \
    --with-gmp=${INSTALL_D}

[[ $? > 0 ]] && error_msg "Configure script for $GNU_MPFR_NAME failed." && exit 1

$GNU_MAKE
[[ $? > 0 ]] && error_msg "Make script for $GNU_MPFR_NAME failed." && exit 1

$GNU_MAKE check
[[ $? > 0 ]] && error_msg "Make check script for $GNU_MPFR_NAME failed." && exit 1

$GNU_MAKE install
[[ $? > 0 ]] && error_msg "Make Install script for $GNU_MPFR_NAME failed." && exit 1

cd $PRJ_ROOT
