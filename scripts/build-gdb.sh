#!/bin/zsh

# Script to download and build GNU GCC for OSX so that we may have access to it for the other tools.
# Default OSX gcc is missing the AVR target functionality

BUILD_GCC_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}
: ${TARGET:=avr}

# import scripts
[[ -z $MESSAGES_SH ]] && source $BUILD_SCRIPT_D/messages.sh
[[ -z $VARIABLES_SH ]] && source $BUILD_SCRIPT_D/variables.sh

# get the src
GNUGDB_SRC_PATH="${DOWNLOAD_D}/${GNU_GDB_SRC}"
GNUGDB_SIG_PATH="${DOWNLOAD_D}/${GNU_GDB_SRC}.sig"
[[ ! -a ${GNUGDB_SRC_PATH} ]] && curl ${GNU_GDB_SRC_URL} -o ${GNUGDB_SRC_PATH}
[[ ! -a ${GNUGDB_SIG_PATH} ]] && curl ${GNU_GDB_SIG_URL} -o ${GNUGDB_SIG_PATH}

#verify
[[ $VERIFY = "true" ]] && gpg  --verify ${GNUGDB_SIG_PATH} ${GNUGDB_SRC_PATH} && if [ ! $? -eq 0 ] ; then ;
     error_msg "GNUGDB SRC TAR did not match the sig file. Error $?" ; fi

cd $BUILD_D

[[ ! -d $BUILD_D/$GNU_GDB_NAME ]] && tar -xvf $GNUGDB_SRC_PATH

[[ ! -d $GNU_GDB_BUILD_D ]] && mkdir $GNU_GDB_BUILD_D
cd $GNU_GDB_BUILD_D 

eval ../configure --prefix=$INSTALL_D --target=${TARGET} --enable-languages=c,c++ \
    --disable-nls --disable-libssp --with-dwarf2

[[ $? > 0 ]] && error_msg "Configure script for $GNU_GDB_NAME failed." && exit 1

$GNU_MAKE
[[ $? > 0 ]] && error_msg "Make script for $GNU_GDB_NAME failed." && exit 1

$GNU_MAKE check
[[ $? > 0 ]] && error_msg "Make check script for $GNU_GDB_NAME failed." && exit 1

$GNU_MAKE install
[[ $? > 0 ]] && error_msg "Make Install script for $GNU_GDB_NAME failed." && exit 1

cd $PRJ_ROOT
