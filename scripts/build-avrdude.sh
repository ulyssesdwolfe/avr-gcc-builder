#!/bin/zsh

# Script to download and build AVR LIB C for OSX so that we may have access to it for the other tools.

BUILD_AVRDUDE_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}
: ${TARGET:=avr}

# import scripts
[[ -z $MESSAGES_SH ]] && source $BUILD_SCRIPT_D/messages.sh
[[ -z $VARIABLES_SH ]] && source $BUILD_SCRIPT_D/variables.sh

# get the src
AVRDUDE_SRC_PATH="${DOWNLOAD_D}/${AVRDUDE_SRC}"
AVRDUDE_SIG_PATH="${DOWNLOAD_D}/${AVRDUDE_SRC}.sig"
[[ ! -a ${AVRDUDE_SRC_PATH} ]] && curl ${AVRDUDE_SRC_URL} -o ${AVRDUDE_SRC_PATH}
[[ ! -a ${AVRDUDE_SIG_PATH} ]] && curl ${AVRDUDE_SIG_URL} -o ${AVRDUDE_SIG_PATH}

#verify
[[ $VERIFY = "true" ]] && gpg  --verify ${AVRDUDE_SIG_PATH} ${AVRDUDE_SRC_PATH} && if [ ! $? -eq 0 ] ; then ;
     error_msg "AVRDUDE SRC TAR did not match the sig file. Error $?" ; fi

cd $BUILD_D

[[ ! -d $BUILD_D/$AVRDUDE_NAME ]] && tar -xvf $AVRDUDE_SRC_PATH

[[ ! -d $AVRDUDE_BUILD_D ]] && mkdir $AVRDUDE_BUILD_D
cd $AVRDUDE_BUILD_D 

eval ../configure --prefix=$INSTALL_D
[[ $? > 0 ]] && error_msg "Configure script for $AVRDUDE_NAME failed." && exit 1

$GNU_MAKE
[[ $? > 0 ]] && error_msg "Make script for $AVRDUDE_NAME failed." && exit 1

$GNU_MAKE check
[[ $? > 0 ]] && error_msg "Make check script for $AVRDUDE_NAME failed." && exit 1

$GNU_MAKE install
[[ $? > 0 ]] && error_msg "Make Install script for $AVRDUDE_NAME failed." && exit 1

cd $PRJ_ROOT
