#!/bin/zsh

# Script to download and build GNU MPC LIB for OSX so that we may have access to it for the other tools.
# Default OSX MPC LIB is missing the AVR target functionality

BUILD_LIBMPC_SH_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}
: ${TARGET:=avr}

# import scripts
[[ -z $MESSAGES_SH ]] && source $BUILD_SCRIPT_D/messages.sh
[[ -z $VARIABLES_SH ]] && source $BUILD_SCRIPT_D/variables.sh

# get the src
LIBMPC_SRC_PATH="${DOWNLOAD_D}/${GNU_MPC_SRC}"
LIBMPC_SIG_PATH="${DOWNLOAD_D}/${GNU_MPC_SRC}.sig"
[[ ! -a ${LIBMPC_SRC_PATH} ]] && curl ${GNU_MPC_SRC_URL} -o ${LIBMPC_SRC_PATH}
[[ ! -a ${LIBMPC_SIG_PATH} ]] && curl ${GNU_MPC_SIG_URL} -o ${LIBMPC_SIG_PATH}

#verify
[[ $VERIFY = "true" ]] && gpg  --verify ${LIBMPC_SIG_PATH} ${LIBMPC_SRC_PATH} && if [ ! $? -eq 0 ] ; then ;
     error_msg "LIBMPC SRC TAR did not match the sig file. Error $?" ; fi

cd $BUILD_D

[[ ! -d $BUILD_D/$GNU_MPC_NAME ]] && tar -xvf $LIBMPC_SRC_PATH

[[ ! -d $GNU_MPC_BUILD_D ]] && mkdir $GNU_MPC_BUILD_D
cd $GNU_MPC_BUILD_D 

eval ../configure --prefix=$INSTALL_D --target=${TARGET} --disable-shared --enable-static \
    --with-gmp="${INSTALL_D}" 
    --with-mpfr="${INSTALL_D}"

[[ $? > 0 ]] && error_msg "Configure script for $GNU_MPC_NAME failed." && exit 1

$GNU_MAKE
[[ $? > 0 ]] && error_msg "Make script for $GNU_MPC_NAME failed." && exit 1

$GNU_MAKE check
[[ $? > 0 ]] && error_msg "Make check script for $GNU_MPC_NAME failed." && exit 1

$GNU_MAKE install
[[ $? > 0 ]] && error_msg "Make Install script for $GNU_MPC_NAME failed." && exit 1

cd $PRJ_ROOT
