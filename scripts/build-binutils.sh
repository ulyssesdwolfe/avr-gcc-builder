#!/bin/zsh

# Script to download and build GNU binutils for OSX so that we may have access to it for the other tools.
# Default OSX binutils is missing the AVR target functionality

BUILD_BINUTILS_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}

source $BUILD_SCRIPT_D/variables.sh
GNU_BINUTILS_LOG_FILE="$LOG_D/$THIS_SCRIPT.sh.log"

# #############################################################################
# Download the source
# #############################################################################

[[ ! -a ${GNU_BINUTILS_SRC_PATH} ]] && curl ${GNU_BINUTILS_SRC_URL} -o ${GNU_BINUTILS_SRC_PATH}
if [ $VERIFY = "true" ] ; then
    [[ ! -a ${GNU_BINUTILS_SIG_PATH} ]] && \
        curl ${GNU_BINUTILS_SIG_URL} -o ${GNU_BINUTILS_SIG_PATH}
fi

# #############################################################################
# Verify the source
# #############################################################################
if [ $VERIFY = "true" ] ; then
    gpg  --verify ${GNU_BINUTILS_SIG_PATH} ${GNU_BINUTILS_SRC_PATH}
    if [ ! $? -eq 0 ] ; then ;
        error_msg "GNUBINUTILS SRC TAR did not match the sig file. Error $?"
    fi
fi

cd $BUILD_D

[[ ! -d $BUILD_D/$GNU_BINUTILS_NAME ]] && tar -xvf $GNUBINUTILS_SRC_PATH

[[ ! -d $GNU_BINUTILS_BUILD_D ]] && mkdir $GNU_BINUTILS_BUILD_D
cd $GNU_BINUTILS_BUILD_D 

[[ ! -e "$GNU_BINUTILS_BUILD_D/Makefile" ]] && eval "$GNU_BINUTILS_CONFIGURE_CMD 2>&1 | tee $GNU_BINUTILS_LOG_FILE"


$GNU_MAKE 2>&1 | tee $GNU_BINUTILS_LOG_FILE

$GNU_MAKE install 2>&1 | tee $GNU_BINUTILS_LOG_FILE

cd $PRJ_ROOT
