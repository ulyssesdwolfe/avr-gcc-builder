#!/bin/zsh

# Script to download and build GNU GCC for OSX so that we may have access to it for the other tools.
# Default OSX gcc is missing the AVR target functionality

BUILD_GCC_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}
: ${TARGET:=avr}

# import scripts
[[ -z $MESSAGES_SH ]] && source $BUILD_SCRIPT_D/messages.sh
[[ -z $VARIABLES_SH ]] && source $BUILD_SCRIPT_D/variables.sh

# get the src
GNUGCC_SRC_PATH="${DOWNLOAD_D}/${GNU_GCC_SRC}"
GNUGCC_SIG_PATH="${DOWNLOAD_D}/${GNU_GCC_SRC}.sig"
[[ ! -a ${GNUGCC_SRC_PATH} ]] && curl ${GNU_GCC_SRC_URL} -o ${GNUGCC_SRC_PATH}
[[ ! -a ${GNUGCC_SIG_PATH} ]] && curl ${GNU_GCC_SIG_URL} -o ${GNUGCC_SIG_PATH}

#verify
[[ $VERIFY = "true" ]] && gpg  --verify ${GNUGCC_SIG_PATH} ${GNUGCC_SRC_PATH} && if [ ! $? -eq 0 ] ; then ;
     error_msg "GNUGCC SRC TAR did not match the sig file. Error $?" ; fi

cd $BUILD_D

[[ ! -d $BUILD_D/$GNU_GCC_NAME ]] && tar -xvf $GNUGCC_SRC_PATH

[[ ! -d $GNU_GCC_BUILD_D ]] && mkdir $GNU_GCC_BUILD_D
cd $GNU_GCC_BUILD_D 

eval ../configure --prefix=$INSTALL_D --target=${TARGET} \
    --disable-nls \
    --disable-libssp \
    --disable-shared \
    --disable-bootstrap \
    --disable-libstdcxx-pch \
    --enable-languages=c,c++ \
    --disable-threads \
    --enable-lto \
    --enable-tls \
    --with-dwarf2 \
    --with-gmp=${INSTALL_D} \
    --with-mpc=${INSTALL_D} \
    --with-mpfr=${INSTALL_D} \
    --with-libelf=${INSTALL_D} \


[[ $? > 0 ]] && error_msg "Configure script for $GNU_GCC_NAME failed." && exit 1

$GNU_MAKE
[[ $? > 0 ]] && error_msg "Make script for $GNU_GCC_NAME failed." && exit 1

$GNU_MAKE install
[[ $? > 0 ]] && error_msg "Make Install script for $GNU_GCC_NAME failed." && exit 1

cd $PRJ_ROOT
