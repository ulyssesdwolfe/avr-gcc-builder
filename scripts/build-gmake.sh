#!/bin/zsh

# Script to download and build GNU make for OSX so that we may have access to it for the other tools.
# Default OSX make is missing functionality we need to build other tools
# #############################################################################
# Initialise the script
# #############################################################################
BUILD_GMAKE_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}


GMAKE_LOG_FILE="$LOG_D/$THIS_SCRIPT.log"


# #############################################################################
# Download the source
# #############################################################################

[[ ! -a ${GNU_MAKE_SRC_PATH} ]] && curl ${GNU_MAKE_SRC_URL} -o ${GNU_MAKE_SRC_PATH}
if [ $VERIFY = "true" ] ; then 
    [[ ! -a ${GNU_MAKE_SIG_PATH} ]] && \
        curl ${GNU_MAKE_SIG_URL} -o "${GNU_MAKE_SIG_PATH}"
fi

# #############################################################################
# Verify the source
# #############################################################################
if [ $VERIFY = "true" ] ; then
    gpg  --verify $GNU_MAKE_SIG_PATH ${GNU_MAKE_SRC_PATH}
    if [ ! $? -eq 0 ] ; then
        error_msg "GNU_MAKE SRC TAR did not match the sig file. Error $?"
    fi
fi


# #############################################################################
# Decompress the source
# #############################################################################
cd $BUILD_D
[[ ! -d "$BUILD_D/$GNU_MAKE_NAME" ]] && tar -xvf $GNU_MAKE_SRC_PATH

[[ ! -d $GNU_MAKE_BUILD_D ]] && mkdir $GNU_MAKE_BUILD_D
cd $GNU_MAKE_BUILD_D

[[ ! -e "$GNU_MAKE_BUILD_D/Makefile" ]] && eval "$GNU_MAKE_CONFIGURE_CMD 2>&1 | tee $GMAKE_LOG_FILE" # run the configuration

make 2>&1 | tee $GMAKE_LOG_FILE

make install 2>&1 | tee $GMAKE_LOG_FILE


cd $PRJ_ROOT
