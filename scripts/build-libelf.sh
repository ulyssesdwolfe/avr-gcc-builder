#!/bin/zsh

# Script to download and build LIB ELF for OSX so that we may have access to it for the other tools.
# Default OSX libelf is missing the AVR target functionality

BUILD_LIBELF_SH=true
THIS_SCRIPT=${0##*/}
BUILD_SCRIPT_D=${0%$THIS_SCRIPT}

source $BUILD_SCRIPT_D/variables.sh


LIBELF_LOG_FILE="$LOG_D/$THIS_SCRIPT.log"

# #############################################################################
# Download the source
# #############################################################################

[[ ! -a ${LIBELF_SRC_PATH} ]] && curl ${LIBELF_SRC_URL} -o ${LIBELF_SRC_PATH}
if [ $VERIFY = "true" ] ; then 
    [[ ! -a ${LIBELF_SIG_PATH} ]] && \
        curl ${LIBELF_SIG_URL} -o ${LIBELF_SIG_PATH}
fi

# #############################################################################
# Verify the source
# #############################################################################
if [ $VERIFY = "true" ] ; then
    gpg  --verify ${LIBELF_SIG_PATH} ${LIBELF_SRC_PATH}
    if [ ! $? -eq 0 ] ; then
        error_msg "LIBELF SRC TAR did not match the sig file. Error $?"
    fi
fi

cd $BUILD_D

[[ ! -d $BUILD_D/$LIBELF_NAME ]] && tar -xvf $LIBELF_SRC_PATH

[[ ! -d $LIBELF_BUILD_D ]] && mkdir $LIBELF_BUILD_D
cd $LIBELF_BUILD_D 
echo $(pwd)

[[ ! -e "$LIBELF_BUILD_D/Makefile" ]] && eval "$LIBELF_CONFIGURE_CMD 2>&1 | tee $LIBELF_LOG_FILE"


$GNU_MAKE 2>&1 | tee $LIBELF_LOG_FILE

$GNU_MAKE check 2>&1 | tee $LIBELF_LOG_FILE

$GNU_MAKE install 2>&1 | tee $LIBELF_LOG_FILE

cd $PRJ_ROOT
