#!/bin/zsh

##### INITIALISATION #####
PRJ_ROOT=$(pwd)
: ${PREFIX:=install-dir}
: ${BUILD_D:="$PRJ_ROOT/build-dir"}
: ${DOWNLOAD_D:="$PRJ_ROOT/downloads"}
: ${ENV_FILE:=avrgccbuilder.conf}
: ${VERIFY:=false}
: ${INSTALL_D:="$PRJ_ROOT/$PREFIX"}

BUILD_SCRIPT_D="${PRJ_ROOT}/scripts"
ENV_FILE="~/.config/avrgccbuilder/${ENV_FILE}"
LOG_D="$PRJ_ROOT/logs"
LOG_FILE="$LOG_D/build-all.sh.log"
VARIABLES=(PRJ_ROOT BUILD_D INSTALL_D BUILD_SCRIPT_D DOWNLOAD_D PREFIX LOGS ENV_FILE)

#source some helper scripts
source ${BUILD_SCRIPT_D}/messages.sh
source ${BUILD_SCRIPT_D}/variables.sh



# mkdir directories
[ ! -d ${DOWNLOAD_D} ] && mkdir $DOWNLOAD_D
[ ! -d ${BUILD_D} ] && mkdir $BUILD_D
[ ! -d ${LOG_D} ] && mkdir $LOG_D
[ ! -d "${HOME}/.config/avrgccbuilder" ] && mkdir "${HOME}/.config/avrgccbuilder"

#remove logs
[ ! -z $BUILDER_DEBUG ] && print_variables ${VARIABLES} 2>&1 | tee $LOG_FILE

# #############################################################################
# Build the shit
# #############################################################################
source $BUILD_SCRIPT_D/build-gmake.sh
msg "Script : 'build-gmake.sh' returned: $?"
[ -z $GNU_MAKE ] && GNU_MAKE="${INSTALL_D}/bin/make"
PROJECT_VARS="$PROJECT_VARS GNU_MAKE=$INSTALL_D/bin/make"

source $BUILD_SCRIPT_D/build-binutils.sh
msg "Script: 'build-binutils.sh' returned: $?"

source $BUILD_SCRIPT_D/build-libelf.sh
msg "Script: 'build-libelf.sh' returned: $?"
exit 123
source $BUILD_SCRIPT_D/build-libgmp.sh
msg "Script: 'build-libgmp.sh' returned: $?"

source $BUILD_SCRIPT_D/build-libmpfr.sh
msg "Script: 'build-libmpfr.sh' returned: $?"

source $BUILD_SCRIPT_D/build-libmpc.sh
msg "Script: 'build-libmpc.sh' returned: $?"

source $BUILD_SCRIPT_D/build-gdb.sh
msg "Script: 'build-libgdb.sh' returned: $?"

source $BUILD_SCRIPT_D/build-gcc.sh
msg "Script: 'build-gcc.sh' returned: $?"

source $BUILD_SCRIPT_D/build-avrlibc.sh
msg "Script: 'build-avrlibc.sh' returned: $?"

source $BUILD_SCRIPT_D/build-avrdude.sh
msg "Script: 'build-avrdude.sh' returned: $?"

zsh ${AVARICE_VARS} ${PROJECT_VARS} $BUILD_SCRIPT_D/build-avarice.sh
msg "build-avarice returned: $?"

